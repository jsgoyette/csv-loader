<?php

class CSV_Loader
{
    /**
     * @var array
     */
    public $fields;

    /**
     * @var array of arrays
     */
    public $lines;

    /**
     * @var array of objects
     */
    public $records;

    /**
     * @param  string $filename name of csv file
     */
    public function __construct($filename)
    {
        $file = fopen($filename, 'r');
        $this->fields = $this->cleanFieldNames(fgetcsv($file));
        $this->lines = array();
        while (($line = fgetcsv($file)) !== false) {
            $this->lines[] = $this->cleanLineValues($line);
        }
        fclose($file);
    }

    /**
     * @param  array $header
     * @return array
     */
    private function cleanFieldNames($header)
    {
        foreach ($header as $n => $field) {
            $field = trim(strtolower($field));
            $header[$n] = preg_replace('/\s+/', '_', $field);
        }
        return $header;
    }

    /**
     * @param  array $line
     * @return array
     */
    private function cleanLineValues($line)
    {
        $clean = array();
        foreach ($line as $field) {
            $clean[] = trim($field);
        }
        return $clean;
    }

    /**
     * @param  bool whether or not to return objects or arrays
     * @return array of objects or arrays
     */
    public function getArray($object = false)
    {
        $this->records = array();
        foreach ($this->lines as $n => $line) {
            $l = $object ? new stdClass() : array();
            foreach ($this->fields as $k => $field) {
                if ($object)
                    $l->{$field} = $line[$k];
                else $l[$field] = $line[$k];
            }
            $this->records[] = $l;
        }
        return $this->records;
    }

    /**
     * @return array of objects
     */
    public function getObjectArray()
    {
        return $this->getArray(true);
    }

    /**
     * @param  string $tablename
     * @return string
     */
    public function getInsertQuery($tablename)
    {
        $sql = "INSERT INTO $tablename (";
        $sql .= implode(', ', $this->fields);
        $sql .= ") VALUES ";
        $tmp = array();
        foreach ($this->lines as $k => $line) {
            $tmp[] = "('".implode("','", $line)."')";
        }
        $sql .= implode(',', $tmp);

        return $sql;
    }

}
